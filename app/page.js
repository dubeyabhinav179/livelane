"use client"

import React, { useState } from 'react';
import {Header, Homepage} from '@/components/index'


const Page = () => {
  const [videoPath, setVideoPath] = useState('');
  const [error, setError] = useState('');

  const handleMediaUpload = async (e) => {
    const file = e.target.files[0];

    const formData = new FormData();
    formData.append('file', file);

    try {
      const response = await fetch('http://127.0.0.1:5000/', {
        method: 'POST',
        body: formData,
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData.error);
      }

      // Assuming the video file is always named 'output_video.mp4'
      const path = 'C:/Users/Dubey Abhinav/Desktop/React Projects/college project/output_video.mp4';

      setVideoPath(path);
      setError('');
    } catch (error) {
      setError(error.message);
      setVideoPath('');
    }
  };

  return (
    <>
    <Header/>
    <Homepage/>
    </>
  );
};

export default Page;

