import { Inter } from "next/font/google";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: 'Live Lane Detection App',
  description: 'Lane Detection is a computer vision task that involves identifying the boundaries of driving lanes in a video or image of a road scene.',
  openGraph: {
    images: [
      {
        url: 'https://opengraph.b-cdn.net/production/documents/95595bf8-7470-4aaf-8e17-bcc83d41f8ac.png?token=TCSEPhck6joq77cOZ0yh2Syl5Ot7ubXSfV_MQIIMzRM&height=620&width=1200&expires=33242367283',
        alt: 'LiveLaneDetection opengraph Image',
      },
    ],
  },
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
