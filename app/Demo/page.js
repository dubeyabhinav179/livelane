"use client"

import React, { useState } from 'react';
import { Header } from '@/components/index';
import greencircle from '@/public/greencircle.png'
import yellowcircle from '@/public/yellowcircle.png'
import Image from 'next/image';
import loadingGif from '@/public/catto1.gif'; // Import your loading GIF

const Page = () => {
    const [videoPath, setVideoPath] = useState('');
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(false);
    const [videoReloadKey, setVideoReloadKey] = useState(0);

    const handleMediaUpload = async (e) => {
        const file = e.target.files[0];
        const formData = new FormData();
        formData.append('file', file);

        try {
            setLoading(true);
            const response = await fetch('http://127.0.0.1:5000/', {
                method: 'POST',
                body: formData,
            });

            if (!response.ok) {
                const errorData = await response.json();
                throw new Error(errorData.error);
            }

            const responseData = await response.json();
            const path = responseData.video_path;

            setVideoPath(path);
            setError('');
            setVideoReloadKey(prevKey => prevKey + 1);
        } catch (error) {
            setError(error.message);
            setVideoPath('');
        } finally {
            setLoading(false);
        }
    };

    const handleDragOver = (e) => {
        e.preventDefault();
    };

    const handleDrop = (e) => {
        e.preventDefault();
        const file = e.dataTransfer.files[0];
        handleMediaUpload({ target: { files: [file] } });
    };

    return (
        <>
            <Header />
            <div className='z-0 absolute left-8 top-40 sm:absolute sm:top-10 sm:left-20'>
                <Image className=''
                    src={greencircle}
                    width={400}
                    height={100}
                    alt="Picture of green circle"
                />
            </div>

            <div className='z-0 absolute right-80 top-8 sm:left-0'>
                <Image className='-mt-8'
                    src={yellowcircle}
                    width={400}
                    height={100}
                    alt="Picture of green circle"
                />
            </div>

            <div
                className='flex justify-center flex-col items-center px-24 py-12 sm:px-4 sm:justify-start'
                onDragOver={handleDragOver}
                onDrop={handleDrop}
            >
                <div className="col-span-full">
                    <label htmlFor="file-upload" className="block text-md text-center font-medium leading-6 text-gray-900 pb-4 cursor-pointer">
                        Live Features are under Progress. Till then, please use a Video. <br></br> ( Use a Straight Cam View Road Lane Video )
                        <input
                            id="file-upload"
                            name="file-upload"
                            type="file"
                            className="sr-only"
                            onChange={handleMediaUpload}
                        />
                    </label>
                    <div className="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10">
                        <div className="text-center">
                            <svg className="mx-auto h-12 w-12 text-gray-300" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                                <path fillRule="evenodd" d="M1.5 6a2.25 2.25 0 012.25-2.25h16.5A2.25 2.25 0 0122.5 6v12a2.25 2.25 0 01-2.25 2.25H3.75A2.25 2.25 0 011.5 18V6zM3 16.06V18c0 .414.336.75.75.75h16.5A.75.75 0 0021 18v-1.94l-2.69-2.689a1.5 1.5 0 00-2.12 0l-.88.879.97.97a.75.75 0 11-1.06 1.06l-5.16-5.159a1.5 1.5 0 00-2.12 0L3 16.061zm10.125-7.81a1.125 1.125 0 112.25 0 1.125 1.125 0 01-2.25 0z" clipRule="evenodd" />
                            </svg>
                            <div className="mt-4 flex text-sm leading-6 text-gray-600">
                                <label htmlFor="file-upload" className="relative cursor-pointer rounded-md bg-white font-semibold text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-indigo-500">
                                    <span>Upload a file</span>
                                    <input id="file-upload" name="file-upload" type="file" className="sr-only" onChange={handleMediaUpload} />
                                </label>
                                <p className="pl-1">or drag and drop</p>
                            </div>
                            <p className="text-xs leading-5 text-gray-600">MP4 · MOV · WMV up to 30MB</p>
                        </div>
                    </div>
                </div>

                {loading && (
                    <div className='pt-12 flex flex-col items-center'>
                        <p className="mb-4">Please wait for 5-10 Sec, Your result is on the way! Processing...</p>
                        <div className="flex justify-center">
                            <Image src={loadingGif} alt="Loading" />
                        </div>
                    </div>
                )}

                {videoPath && !loading && (
                    <video className='mt-8 p-4 shadow-lg bg-yellow-300 rounded-lg' width="600" controls>
                        <source src={`http://127.0.0.1:5000/output?${Date.now()}`} type="video/mp4" />
                        Your browser does not support the video tag.
                    </video>
                )}


                {error && <p style={{ color: 'red' }}>{error}</p>}
            </div>
        </>
    );
};

export default Page;
