import React from 'react'
import Image from 'next/image'
import greencircle from '@/public/greencircle.png'
import yellowcircle from '@/public/yellowcircle.png'
import groupclients from '@/public/groupclients.png'
import Link from 'next/link'

const page = () => {
    return (
        <>
            <Link href='/Demo'>
                <div>
                    <div className='flex justify-center'>
                        <Image className='-mt-24 ml-32 sm:ml-0'
                            src={yellowcircle}
                            width={800}
                            height={100}
                            alt="Picture of yellow circle"
                        />
                    </div>


                    <div className='z-0 absolute left-44'>
                        <Image className='-mt-64'
                            src={greencircle}
                            width={500}
                            height={100}
                            alt="Picture of green circle"
                        />
                    </div>



                    <div className='none px-28 -mt-72 md:-mt-40 md:px-4 sm:px-4 sm:-mt-16 sm:flex sm:flex-col sm:justify-center sm:items-center'>
                        <h1 className='text-6xl font-bold md:text-5xl sm:text-4xl'>
                            Bring the Safety you want <br></br>to see. Made by <br></br>Abhinav Dubey
                        </h1>
                        <p className='mt-8'>
                            Inspired by Live Lane Detection idea, not advertising, Lights produces Paths, <br></br>live Detection & interactive experiences shaped to track and provide a <br></br>safe Journey. <br></br><br></br>
                            <span className='font-bold text-xl text-blue-700'>Click AnyWhere for a Quick Demo </span>
                        </p>

                        <p className='mt-8 hidden sm:block'>
                        A lane line usually spans the entire image, which requires the network to have a large enough receptive field.
                        </p>
                    </div>



                    <div className='flex justify-center'>
                        <Image className='-mt-80 md:-mt-12 sm:-mt-0 absolute bottom-30 sm:bottom-0'
                            src={groupclients}
                            style={{
                                width: '100vw',
                                objectFit: 'contain',
                            }}
                            alt="Picture of clients and people in circle"
                        />
                    </div>


                </div>
            </Link>
        </>
    )
}

export default page
