import React from 'react'
import Link from 'next/link'

const page = () => {
    return (
        <>


            <header className=''>
                <nav class="bg-white border-gray-200 px-4 lg:px-6 py-8">
                    <div class="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
                        <Link href="/" class="flex items-center">
                            <span class="self-center text-xl font-medium whitespace-nowrap">LiveLaneDetection</span>
                        </Link>
                        <div class="hidden justify-between items-center w-full lg:flex lg:w-auto lg:order-1" id="mobile-menu-2">
                            <ul class="flex flex-col mt-4 font-medium lg:flex-row lg:space-x-8 lg:mt-0">
                                <li>
                                    <Link href='/' class="block py-2 pr-4 pl-3 text-gray-700 rounded bg-primary-700 lg:bg-transparent lg:text-primary-700 lg:p-0" aria-current="page">Home</Link>
                                </li>
                                <li>
                                    <Link href='/About' class="block py-2 pr-4 pl-3 text-gray-700 rounded bg-primary-700 lg:bg-transparent lg:text-primary-700 lg:p-0" aria-current="page">About Us</Link>
                                </li>
                                <li>
                                    <Link href='/Demo' class="block py-2 pr-4 pl-3 text-gray-700 rounded bg-primary-700 lg:bg-transparent lg:text-primary-700 lg:p-0" aria-current="page">Demo</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>


        </>
    )
}

export default page
